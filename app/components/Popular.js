import React, { Component } from 'react';
import { fetchPopularRepos } from "../service/utils";
import { SelectedLanguage } from "./SelectedLanguage";
import { RepoGrid } from "./RepoGrid";

export default class Popular extends Component {
  constructor() {
    super();
    this.state = {
      selectedLanguage: 'All',
      repos: null,
      error: null
    };
    this.selectLanguage = this.selectLanguage.bind(this);
  }

  componentDidMount() {
    fetchPopularRepos(this.state.selectedLanguage)
      .then(response => this.setState({ repos: response }))
      .catch(error => this.setState({ error: error }))
  }

  selectLanguage(event) {
    fetchPopularRepos(event.target.innerText)
      .then(response => this.setState({ repos: response }));
    this.setState({ selectedLanguage: event.target.innerText });
  }

  render() {
    if(this.state.error) {
      return <p>{this.state.error}</p>
    }
    return (
      <div>
        <SelectedLanguage
          onSelect={this.selectLanguage}
          language={this.state.selectedLanguage} />
        <RepoGrid repos={this.state.repos} />
      </div>
    )
  }
}