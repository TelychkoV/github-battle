import React from 'react';

export const RepoGrid = props => (
  <ul className='popular-list'>
    {props.repos ?
      props.repos.map((repo, index) => (
          <li key={repo.name} className='popular-item'>
            <div className='popular-rank'>#{index + 1}</div>
            <ul className='space-list-items'>
              <li>
                <img className='avatar' src={repo.owner.avatar_url} alt="Avatar" />
              </li>
              <li><a href={repo.html_url} target='blank'>{repo.name}</a></li>
              <li>@{repo.owner.login}</li>
              <li>{repo.stargazers_count} start</li>
            </ul>
          </li>
        )
      ) : null}
    </ul>
  );