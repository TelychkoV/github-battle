import React from 'react';

export const SelectedLanguage = props => {
  const languages = ['All', 'Javascript', 'Ruby', 'Java', 'CSS', 'Python'];
  return (
    <ul className='languages'>
      {languages.map((language, index) => (
        <li
          onClick={props.onSelect}
          style={props.language === language ? { color: '#d0021b' } : null}
          key={index}>
           {language}
        </li>
      ))}
    </ul>
  )
};