import React, { Component } from 'react';

export default class PlayerInput extends Component {
  constructor() {
    super();
    this.state = {
      username: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ username: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(this.state)
  }

  render() {
    console.log(this.state);
    return (
      <form className='column' onSubmit={this.handleSubmit}>
        <label className='header' htmlFor="username">{this.props.label}</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.state.username} />
        <button
          className='button'
          disabled={!this.state.username}
          type='submit'>Submit</button>
      </form>
    )
  }
}
