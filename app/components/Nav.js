import React from 'react';
import { NavLink } from 'react-router-dom';

const links = ['Home', 'Battle', 'Popular'];

export const Nav = () => (
  <ul className='nav'>
    {links.map((link, index) => (
      <li key={index}>
        <NavLink
          exact
          activeClassName='active'
          to={link === 'Home' ? '/' : `/${link.toLowerCase()}`}>
          {link}</NavLink>
      </li>
    ))}
  </ul>
);