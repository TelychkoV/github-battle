import React, { Component } from 'react';
import PlayerInput from './PlayerInput';

export default class Battle extends Component {
  constructor() {
    super();
    this.state = {
      value: ''
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    console.log(this.state);
    return (
      <div className='row'>
        <PlayerInput
          label='Player One' />
        <PlayerInput
          label='Player Two' />
      </div>
    )
  }
}
